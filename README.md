# Dummy Operator

This operator manages "Dummy" resources.

Each dummy has a `message`, which is specified by the creator of the dummy.

The operator ensures that the `status.specEcho` field always matches the `message`.

It also manages a single `nginx` pod, the status of which is exposed as `status.podStatus`.

See [Usage](#usage) below for more details.

## Installation

Prerequisites:
- A running kubernetes cluster, e.g. minikube
- A local installation of the `operator-sdk` command

Steps:
1. Check if OLM is installed on the target cluster:
   ```
   operator-sdk olm status
   ```
2. If there are errors, install it (otherwise skip this step)
   ```
   operator-sdk olm install
   ```
3. Run the bundle for Dummy operator
   ```
   operator-sdk run bundle docker.io/nilclass/nothing-to-see-here:bundle-latest
   ```

Wait for the last command to complete, then run this command to watch the logs:
```
kubectl get pods -o name | grep dummy-operator | xargs kubectl logs -c manager -f
```

## Usage

### Creating a new Dummy

A dummy can be created from a manifest like this one:
```yaml
apiVersion: dummy.interview.com/v1alpha1
kind: Dummy
metadata:
  name: dummy1
  namespace: default
spec:
  message: "I'm just a dummy"
```

To create it take these steps:
1. Copy the contents from above into a file named `dummy1.yaml`
2. Create the dummy by running:
   ```
   kubectl create -f dummy1.yaml
   ```

To verify that the dummy was created, run
```
kubectl get dummies
```

The output should look similar to this:
```
NAME     AGE
dummy1   1m
```

### Changing the "message" of a Dummy

A dummy's message can be changed by patching the respective resource:
```
kubectl patch dummy dummy1 --type=merge -p '{"spec":{"message":"New Message"}}'
```

The controller will update the message accordingly, as can be seen by running:
```
kubectl get dummy dummy1 -o 'jsonpath={.status.specEcho}'
```

### Observing the dummy's pod

While a dummy exists, it always keeps up a single pod, running a plain `nginx` image.

The pod has a `dummy` label, equal to the name of the dummy it was created for.

It can be observed like this:
```
$ kubectl get pods -l dummy=dummy1
NAME           READY   STATUS    RESTARTS   AGE
dummy1-4bzfp   1/1     Running   0          13s
```

If the pod is deleted, a new one will be created in it's place:
```
$ kubectl delete pod -l dummy=dummy1
pod "dummy1-4bzfp" deleted
$ kubectl get pods -l dummy=dummy1
NAME           READY   STATUS              RESTARTS   AGE
dummy1-dwntx   0/1     ContainerCreating   0          2s
```

When a dummy is deleted, the corresponding pod is deleted along with it:
```
$ kubectl delete dummy dummy1
dummy.dummy.interview.com "dummy1" deleted
$ kubectl get pods -l dummy=dummy1
No resources found in default namespace.
```
