/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	dummyv1alpha1 "github.com/nilclass/dummy-operator/api/v1alpha1"
)

// DummyReconciler reconciles a Dummy object
type DummyReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

// Update SpecEcho, if it is different from Message.
//
// Returns true, if changes were made to `dummy`.
func (r *DummyReconciler) echoMessage(dummy *dummyv1alpha1.Dummy, changed *bool) {
	if dummy.Status.SpecEcho != dummy.Spec.Message {
		dummy.Status.SpecEcho = dummy.Spec.Message
		*changed = true
	}
}

// Initialize the PodStatus to it's default, if it is not set.
//
// Returns true, if changes were made to `dummy`.
func (r *DummyReconciler) initPodStatus(dummy *dummyv1alpha1.Dummy, changed *bool) {
	if dummy.Status.PodStatus == "" {
		dummy.Status.PodStatus = "Pending"
		*changed = true
	}
}

// Initialize a `Pod` object for the given dummy
func (r *DummyReconciler) initializePod(dummy *dummyv1alpha1.Dummy) *corev1.Pod {
	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: fmt.Sprintf("%s-", dummy.Name),
			Namespace:    dummy.Namespace,
			Labels: map[string]string{
				"dummy": dummy.Name,
			},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  "nginx",
					Image: "nginx:stable",
				},
			},
		},
	}

	ctrl.SetControllerReference(dummy, pod, r.Scheme)

	return pod
}

//+kubebuilder:rbac:groups=dummy.interview.com,resources=dummies,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dummy.interview.com,resources=dummies/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dummy.interview.com,resources=dummies/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=pods,verbs=list;get;create;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Dummy object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *DummyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	dummy := &dummyv1alpha1.Dummy{}
	err := r.Get(ctx, req.NamespacedName, dummy)
	if err != nil {
		if errors.IsNotFound(err) {
			logger.Info("Dummy not found (deleted?). Ignoring.")
			return ctrl.Result{}, nil
		} else {
			return ctrl.Result{}, err
		}
	}

	logger.Info("Reconciling", "Message", dummy.Spec.Message, "PodStatus", dummy.Status.PodStatus)

	// Check if SpecEcho or PodStatus need update / initialization
	updateNeeded := false
	r.echoMessage(dummy, &updateNeeded)
	r.initPodStatus(dummy, &updateNeeded)
	if updateNeeded {
		err := r.Status().Update(ctx, dummy)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	// List all pods that are related to this dummy
	podList := &corev1.PodList{}
	listOpts := []client.ListOption{
		client.MatchingLabels{"dummy": dummy.Name},
	}
	err = r.List(ctx, podList, listOpts...)
	if err != nil {
		return ctrl.Result{}, err
	}

	// If no matching pods are found, create one
	if len(podList.Items) == 0 {
		pod := r.initializePod(dummy)
		logger.Info("Creating pod")
		err = r.Create(ctx, pod)
		if err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	// More pods running than expected, delete one
	if len(podList.Items) > 1 {
		logger.Info("Deleting extraneous pod")
		err = r.Delete(ctx, &podList.Items[0])
		if err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	// Now only a single pod is left. Ensure that our PodStatus matches it's Status.Phase.
	pod := &podList.Items[0]
	if pod.Status.Message != dummy.Status.PodStatus {
		dummy.Status.PodStatus = string(pod.Status.Phase)
		logger.Info(fmt.Sprintf("Updating PodStatus=%s", pod.Status.Phase))
		err = r.Status().Update(ctx, dummy)
		if err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DummyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&dummyv1alpha1.Dummy{}).
		Owns(&corev1.Pod{}).
		Complete(r)
}
