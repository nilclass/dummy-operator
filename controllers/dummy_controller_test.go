package controllers

import (
	"context"
	"fmt"
	"os"
	"time"

	dummyv1alpha1 "github.com/nilclass/dummy-operator/api/v1alpha1"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var _ = Describe("Dummy controller", func() {
	Context("Dummy controller test", func() {

		const DummyName = "test-dummy"
		const NamespaceName = "test-namespace"

		ctx := context.Background()

		var namespace *corev1.Namespace

		typeNamespaceName := types.NamespacedName{Name: DummyName, Namespace: NamespaceName}

		BeforeEach(func() {
			namespace = &corev1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      NamespaceName,
					Namespace: NamespaceName,
				},
			}
			By("creating namespace")
			err := k8sClient.Create(ctx, namespace)
			Expect(err).To(Not(HaveOccurred()))

			By("setting DUMMY_IMAGE env var")
			err = os.Setenv("DUMMY_IMAGE", "interview.com/image:test")
			Expect(err).To(Not(HaveOccurred()))
		})

		AfterEach(func() {
			By("deleting namespace")
			err := k8sClient.Delete(ctx, namespace)
			Expect(err).To(Not(HaveOccurred()))

			By("unsetting DUMMY_IMAGE env var")
			err = os.Unsetenv("DUMMY_IMAGE")
			Expect(err).To(Not(HaveOccurred()))
		})

		// FIXME: this test case is very long, and could easily be split across multiple cases.
		//   However, doing so caused some errors related to the namespace deletion in AfterEach.
		It("ensures that status.specEcho == spec.message, and status.podStatus reflects the status of the pod", func(specCtx SpecContext) {
			By("Creating custom resource of kind Dummy")
			dummy := &dummyv1alpha1.Dummy{
				ObjectMeta: metav1.ObjectMeta{
					Name:      DummyName,
					Namespace: NamespaceName,
				},
				Spec: dummyv1alpha1.DummySpec{
					Message: "Initial Message",
				},
			}
			err := k8sClient.Create(ctx, dummy)
			Expect(err).To(Not(HaveOccurred()))

			By("Waiting for resource to be created")
			Eventually(func() error {
				found := &dummyv1alpha1.Dummy{}
				return k8sClient.Get(ctx, typeNamespaceName, found)
			}).WithContext(specCtx).Should(Succeed())

			dummyReconciler := &DummyReconciler{
				Client: k8sClient,
				Scheme: k8sClient.Scheme(),
			}

			_, err = dummyReconciler.Reconcile(ctx, reconcile.Request{
				NamespacedName: typeNamespaceName,
			})
			Expect(err).To(Not(HaveOccurred()))

			err = k8sClient.Get(ctx, typeNamespaceName, dummy)
			Expect(err).To(Not(HaveOccurred()))

			By("Checking if specEcho and podStatus were updated")
			Expect(dummy.Status).To(Equal(dummyv1alpha1.DummyStatus{
				SpecEcho:  "Initial Message",
				PodStatus: "Pending",
			}))

			By("Patching message")
			updated := *dummy
			updated.Spec.Message = "New Message"
			By(fmt.Sprintf("updated: %v, dummy: %v", updated, dummy))
			err = k8sClient.Patch(ctx, &updated, client.MergeFrom(dummy))
			Expect(err).To(Not(HaveOccurred()))

			By("Waiting for message to be updated")
			Eventually(func() string {
				found := &dummyv1alpha1.Dummy{}
				err := k8sClient.Get(ctx, typeNamespaceName, found)
				Expect(err).To(Not(HaveOccurred()))
				return found.Spec.Message
			}).WithContext(specCtx).Should(Equal("New Message"))

			By("Second reconciliation")
			_, err = dummyReconciler.Reconcile(ctx, reconcile.Request{
				NamespacedName: typeNamespaceName,
			})
			Expect(err).To(Not(HaveOccurred()))

			By("Checking if specEcho was updated")
			err = k8sClient.Get(ctx, typeNamespaceName, dummy)
			Expect(err).To(Not(HaveOccurred()))
			Expect(dummy.Status.SpecEcho).To(Equal("New Message"))

			By("Checking if a pod was created")
			pods := &corev1.PodList{}
			err = k8sClient.List(ctx, pods, client.MatchingLabels{"dummy": DummyName})
			Expect(err).To(Not(HaveOccurred()))
			Expect(len(pods.Items)).To(Equal(1))

			By("Checking if the pod is owned by the correct resources")
			ref := pods.Items[0].ObjectMeta.OwnerReferences[0]
			Expect(ref.Kind).To(Equal("Dummy"))
			Expect(ref.Name).To(Equal(DummyName))

			By("Delete the dummy")
			err = k8sClient.Delete(ctx, dummy)
			Expect(err).To(Not(HaveOccurred()))

			Eventually(func() error {
				found := &dummyv1alpha1.Dummy{}
				err := k8sClient.Get(ctx, typeNamespaceName, found)
				return err
			}).WithContext(specCtx).Should(Not(Succeed()))

			By("Final reconciliation, after dummy was deleted")
			_, err = dummyReconciler.Reconcile(ctx, reconcile.Request{
				NamespacedName: typeNamespaceName,
			})
			Expect(err).To(Not(HaveOccurred()))
		}, SpecTimeout(time.Second*10))
	})
})
